﻿#include "../src/saper.hpp"
#include "gmock/gmock.h"

using namespace::testing;

class TestSaper : public Test
{

public:
    Saper game;
};

TEST_F(TestSaper, IsThereAMatrix)
{
    game.createMatrix();
    EXPECT_THAT(game(3,3)->value, Eq(0));
}

TEST_F(TestSaper, WeAreAbleToSet10BombsOnTheMatrix)
{
    game.createMatrix();
    game.plantBombs(10);
    int bombs = 0;
    for (auto row = 0; row < 8; ++row)
        for (auto column = 0; column < 8; ++column)
            if (game(row, column)->value == 9)
                bombs+=1;
    EXPECT_EQ(10, bombs);

}

TEST_F(TestSaper, WeAreAbleToSet10BombsOnTheMatrixAndOthersAreLow)
{
    game.createMatrix();
    game.plantBombs(10);
    bool temp = false;
    int bombs = 0;
    for (auto row = 0; row < 8; ++row)
        for (auto column = 0; column < 8; ++column)
        {
            if (game(row, column)->value != 9 &&
                    game(row, column)->value != 0)
             temp = true;
        }
    ASSERT_TRUE(temp);
}

TEST_F(TestSaper, IfGameDisplay)
{
    game.createMatrix();
    game.plantBombs(10);
    game.display();
}

TEST_F(TestSaper, IfGameIsUncoveredByTheBeginning)
{
    game.createMatrix();
    game.plantBombs(10);
    game.display();
}

TEST_F(TestSaper, IfGameHasAConsolAndReceivesInputAndUncover)
{
    game.run();
}

TEST_F(TestSaper, IfFirstUncoverThanCreateMatrix)
{
    game.run();
}

TEST_F(TestSaper, IfFirstUncoverBlankThanUncoverOtherFields)
{
//      0  1  2  3  4  5  6  7
//   0 {0, 0, 0, 1, 0, 0, 0, 0}
//   1 {0, 4, 9, 1, 0, 0, 0, 0}
//   2 {X, 3, 1, 1, 1, 0, 0, 0}
//   3 {0, 2, 1, 0, 1, 0, 0, 0}
//   4 {1, 3, 1, 0, 1, 0, 0, 0}
//   5 {0, 0, 1, 0, 1, 0, 0, 0}
//   6 {0, 0, 1, 1, 1, 0, 0, 0}
//   7 {0, 0, 0, 0, 0, 0, 0, 0}

    game.createMatrix();
    game(2,0)-> discover = true; //setting starting point

    game(4,0)->value = 1;
    game(4,1)->value = 3;
    game(1,1)->value = 4;
    game(1,2)->value = 9;
    game(0,3)->value = 1;


    game(1,3)->value = 1;
    game(3,1)->value = 2;
    game(2,1)->value = 3;



    game.display();
    game.uncover(2,0);
    EXPECT_FALSE(game(1,2)->discover);
    EXPECT_FALSE(game(0,4)->discover);
    EXPECT_TRUE(game(0,3)->discover);

}
