#include "saper.hpp"
#include <random>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <map>
void Saper::createMatrix()
{
    for (auto row = 0; row < size; ++row)
    {
        std::vector<Field*> row_v;
        board.push_back(std::move(row_v));
        for (int column = 0; column < size; ++column)
            board[row].push_back(new Field);
    }
}

void Saper::uncover(int row, int column) //gdzie row i column to współrzędne naszego punktu
{
    //Poniżej uncoverujemy wokół punktu
    for (auto & by_row: {-1, 0, 1})
    {
        for (auto & by_column: {-1, 0, 1})
        {
            int new_row = row + by_row;
            int new_column = column + by_column;

            if(!moving(new_row, new_column)) //sprawdzamy czy punkt sąsiedni istnieje
                continue;
            if (by_row == 0 && by_column == 0) // sprawdzamy czy nie stoimy w miejscu
                continue;
            if (board[new_row][new_column]-> value == 9)
                continue;// bomby nie odsłaniamy
            if (board[new_row][new_column]-> discover == true)
                continue; //nie odsłaniamy czegoś odsłoniętego
            board[new_row][new_column]->discover = true; //odsłaniamy/uncover
            if (board[new_row][new_column]->value == 0)
                uncover(new_row, new_column);//jeśli odsłaniany punkt któego wartość jest równa zero -> wywołujemy funkcję na nim;
        }
    }
}


Saper::Field* Saper::operator ()(int X , int Y)
{
    return board[X][Y];
}


void Saper::plantBombs(int bombs)
{
    std::srand(std::time(NULL));

    while (bombs > 0)
    {
        bool breaker = false;
        int row = rand() % size;
        int column = rand() % size;

        if(board[row][column]->value == 9)
        {
            continue;
        }

        if(board[row][column]->discover == true)
            continue;
        for (auto & by_row: {-1, 0, 1})
        {
            for (auto & by_column: {-1,0, 1})
            {
                int new_row = row + by_row;
                int new_column = column + by_column;
                if(!moving(new_row, new_column))
                    continue;
                if(by_row == 0 && by_column == 0)
                    continue;
                if(board[new_row][new_column]->value == 3)
                {
                    breaker = true; break;
                }
                if(board[new_row][new_column]->value == 9)
                    continue;

                board[new_row][new_column]->value++;
            }
            if (breaker)
                break;
        }
        if (breaker)
            continue;
        bombs--;
        board[row][column]->value = 9;
    }
}

bool Saper::moving(int new_row, int new_column)
{
    if (new_row < 0 ||
            new_row > size - 1 ||
            new_column < 0 ||
            new_column > size - 1)
        return false;
    return true;
}

void Saper::display()
{

    for (auto row = 0; row < size; ++row)
    {
        for (auto column = 0; column < size; ++column)
        {
//            if(board[row][column] -> discover == false)
//                std::cout << "#"  << " ";
//            else
                std::cout << board[row][column] -> value  << " ";
        }
        std::cout << "\n";
    }

    std::cout << "\n\nPut the name of the field: \n";



}

void Saper::run()
{
    createMatrix();
 
    std::string command;
    display();
    //std::cin >> command; na razie hardcodujemy
    command = "A2";
    int row = command[0] - 'A'; // Tutaj bierzemy intową wartość chara i odejmujemy od pierwszej litery alfabetu
    int column = command[1] - '0'; // Analogicznie
 
    board[row][column] -> discover = true; // Odkryte pole startowe
    display();
    plantBombs(10);
    display();
 
    if (board[row][column] == 0) //Jeśli nasze pole jest 0 uruchamiamy uncover - uwaga Piotrka
        uncover(row, column);
    return;
}
