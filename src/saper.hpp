#pragma once
#include <vector>
#include <memory>

class Saper
{
public:
    struct Field
    {
        int value = 0;
        bool discover = false;
    };
    void createMatrix();
    Field* operator ()(int, int);
    void plantBombs(int);
    void setNumbers();
    bool moving(int, int);
    void display();
    ~Saper()
    {
        for (auto & row : board)
            for (auto & column : row)
                delete column;
    }

    void run();
    void uncover(int, int);


private:
    std::vector<std::vector<Field*>> board;
    int size = 8;
    int bombCounter;
};
